from datetime import datetime

import constants


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Logger:
    __metaclass__ = Singleton

    def __init__(self):
        self.file_log = "logs.txt"

    def debug_message(self, msg):
        line = self.create_msg(msg, "[DEBUG]")
        if constants.DEBUG:
            print(line)
            self._wirte_file(line)

    def warning_message(self, msg):
        line = self.create_msg(msg, "[WARNING]")
        print(line)
        self._wirte_file(line)

    def error_message(self, msg):
        from sys import exc_info
        from traceback import format_exc

        line = self.create_msg(msg, "[ERROR]") + str(exc_info()[0]) + str(format_exc()) + "\n"
        self._wirte_file(line)
        print(line)

    def _wirte_file(self, line):
        with open(self.file_log, "a") as logs:
            logs.write(line)

    def create_msg(self, msg, type):
        return "[" + str(datetime.now()) + "] " + type + " " + str(msg).rstrip("\r\n") + "\n"


logger = Logger()
