from constants import network_file_name
from DataHandling.logger import logger

import os
import pickle

def get_ips_and_ports():
    if os.path.exists(os.path.join(os.getcwd(), network_file_name)):
        try:
            with open(network_file_name, mode='rb') as network_file:
                return pickle.load(network_file)
        except:
            logger.error_message("An unexpected error encountered during network file deserialization.")
    else:
        return []

def save_ips_and_ports(ips_and_ports):
    try:
        with open(network_file_name, mode='wb') as network_file:
            pickle.dump(ips_and_ports, network_file)
    except:
        logger.error_message("An unexpected error encountered during network file serialization.")

def append_ips_and_ports(appended_ips_and_ports):
    ips_and_ports = get_ips_and_ports()

    if ips_and_ports is not None:
        ips_and_ports += appended_ips_and_ports
        save_ips_and_ports(ips_and_ports)
    else:
        save_ips_and_ports(appended_ips_and_ports)
