from DataHandling.logger import logger

import pymysql as db


class DBManipulator:
    HOST = 'localhost'
    PORT = 3306

    def __init__(self, db_name, table_name):
        # setup the connection
        self.db_name = db_name
        self.table_name = table_name
        # credentials = validator.get_credentials(db_name)
        credentials = ['root', 'ips@biot']

        if credentials is None:
            logger.debug_message('ACCESS DENIED TO DATABASE.')

        self.get_everything = '*'

        try:
            # self.connection = db.connect(self.HOST, self.USER, self.PASSWORD, self.DB)
            self.connection = db.connect(host= self.HOST, port= self.PORT,user=credentials[0],
                                         passwd=credentials[1], db=self.db_name)
            self.dbHandler = self.connection.cursor()
            del credentials
        except Exception as exception:
            logger.debug_message(exception)

    def terminate_connection(self):
        self.connection.close()

    def commit_changes_to_db(self):
        self.connection.commit()

    def query_db(self, table_name, list_of_fields):
        joined_list_of_fields = ','.join(list_of_fields)

        #########
        # todo: sql injection attacks detection for field names and table name
        #########

        sql_query = "SELECT " + joined_list_of_fields + " FROM " + table_name
        try:
            self.dbHandler.execute(sql_query)
            data = self.dbHandler.fetchall()
            return data

        except Exception as exception:
            logger.debug_message(exception)

    def conditional_query(self, table_name, list_of_fields, condition):
        joined_list_of_fields = ','.join(list_of_fields)

        #########
        # todo: sql injection attacks detection
        #########

        sql_query = "SELECT " + joined_list_of_fields + " FROM " + table_name + " WHERE " + condition
        try:
            self.dbHandler.execute(sql_query)
            data = self.dbHandler.fetchall()
            return data

        except Exception as exception:
            logger.debug_message(exception)

    def insert_into_db(self, table_name, list_of_fields, list_of_values):
        joined_list_of_fields = ','.join(list_of_fields)
        # logger.debug_message (self.mixedListFormatting(listOfValues))
        mixed_list = self.mixed_list_formatting(list_of_values)
        sql_query = "INSERT INTO " + table_name + "(" + joined_list_of_fields + ") VALUES (" + mixed_list + ")"

        try:
            self.dbHandler.execute(sql_query)
            logger.debug_message("Successfully inserted!")
        except Exception as exception:
            logger.debug_message(exception)

    def delete_entry_from_db(self, table_name, condition):
        sql_query = "DELETE FROM " + table_name + " WHERE " + condition

        try:
            self.dbHandler.execute(sql_query)

        except Exception as exception:
            logger.debug_message(exception)

    @staticmethod
    def mixed_list_formatting(list_of_values):
        formatted_values = ''
        for value in list_of_values:
            if type(value) == 'str':
                formatted_values = formatted_values + "'" + value + "'" + ","
            else:
                formatted_values = formatted_values + str(value) + ","

        formatted_values = formatted_values[:-1]  # removes the final comma
        return formatted_values

    def _delete_all_table_data(self):
        sql_query = "DELETE FROM " + self.table_name
        try:
            self.dbHandler.execute(sql_query)

        except Exception as exception:
            logger.debug_message(exception)

    @staticmethod
    def get_ip_table_fields():
        return ['id', 'ip', 'port']


    def get_ips_and_ports(self):
        pass


# EXAMPLE OF MANIPULATOR SYNTAX
if __name__ == "__main__":
    pass