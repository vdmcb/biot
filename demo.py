from Blockchain.blockchain import Blockchain
from Blockchain.data_generator import DataGenerator
from Blockchain.data_pool import DataPool
from Blockchain.miner import Miner
import time

if __name__ == "__main__":
    blockchain = Blockchain()
    data_pool = DataPool()

    # Creating the mining threads


    # Generating some arbitrary data
    data_generator = DataGenerator(data_pool)
    data_generator.start()

    time.sleep(3)
    number_of_miners = 1
    miners = []
    for n in range(number_of_miners):
        miners.append(Miner(n, blockchain, data_pool))
        miners[n].start()

    for n in range(number_of_miners):
        miners[n].join()

    # Iterating through Blockchain
    print("ITERATING THROUGH BLOCKS: ")
    current_block = blockchain.genesis_block
    while current_block is not None:
        print(current_block)
        current_block = current_block.next
