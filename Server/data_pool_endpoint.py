from flask import Flask, request
from flask_restful import Api, Resource, reqparse
import random
from threading import Lock
from queue import Queue
import time

app = Flask(__name__)
api = Api(app)
pending_submissions = []


# class ActionQueue:
#     def __init__(self):
#         self.actions = Queue()
#         self._locked = False
#
#     def queue(self, resolver, action_type, data=None):
#         if action_type == 'get' or action_type == 'post':
#             self.actions.put((action_type, data, resolver))
#             self.resolve_action()
#
#     def resolve_action(self):
#         if self._locked:
#             return
#
#         self._locked = True
#         while not self.actions.empty():
#             resolver, action_type, data = self.actions.get()
#
#             if action_type == 'get':



class DataPool(Resource):
    _instance = None

    def __init__(self):
        if self._instance is None:
            self._instance = self
            # pending_submissions = []
            self.lock = Lock()
            self.max_num_of_transactions = 320
        else:
            raise Exception('Singleton. Cannot create another instance.')

    def get(self):
        time.sleep(3)
        data = self.retrieve_random_data_package()
        print(str(data))
        return data, 200

    def post(self):
        print("POST CALLED")
        parser = reqparse.RequestParser()
        parser = parser.add_argument('Public key')
        parser = parser.add_argument('data')
        parser = parser.add_argument('date')
        parser = parser.add_argument('signature')
        data = parser.parse_args()
        print(str(data))
        if data is not None:
            self.submit_data(data)
            return data, 200
        return None, 400

    @property
    def instance(self):
        if self._instance is None:
            DataPool()
        return self._instance

    def shuffle_submissions(self):
        global pending_submissions
        with self.lock:
            random.shuffle(pending_submissions)

    def submit_data(self, data):
        global pending_submissions
        with self.lock:
            pending_submissions.append(data)
            print('Data appended: ' + str(data))
            print('Signature: ', data['signature'])
            print(len(pending_submissions))

    # The data is already shuffled so the transaction selection order is kept arbitrary
    def retrieve_random_data_package(self):
        global pending_submissions
        num_of_transactions = self.max_num_of_transactions
        with self.lock:
            print(len(pending_submissions))
            if len(pending_submissions) > 0:
                random.shuffle(pending_submissions)
                if len(pending_submissions) >= num_of_transactions:
                    sent_submissions = pending_submissions[:num_of_transactions]
                    pending_submissions = pending_submissions[num_of_transactions:]
                    return sent_submissions

                sent_submissions = pending_submissions[:]
                pending_submissions = []
                return sent_submissions
            return None


if __name__ == '__main__':
    api.add_resource(DataPool, '/')
    app.run(debug=False, port=9987)
