from threading import Thread, Lock
from Crypto.Hash import SHA256
import datetime


class Block:
    def __init__(self, list_of_submissions):
        self.previous_hash = None
        self.block_hash = None
        self.next = None
        self.block_num = 0
        self.nonce = 0
        self.timestamp = datetime.datetime.now()

        self.list_of_submissions = list_of_submissions
        self.submissions_num = len(self.list_of_submissions)

    def hash(self):
        hashing_function = SHA256.new()
        hashing_function.update(
            str(self.nonce).encode('utf-8') +
            str(self.list_of_submissions).encode('utf-8') +
            str(self.previous_hash).encode('utf-8') +
            str(self.timestamp).encode('utf-8') +
            str(self.block_num).encode('utf-8')
        )

        return hashing_function.hexdigest()

    def __str__(self):
        return "Block Hash: " + str(self.hash()) + "\nBlock number: " + str(self.block_num) + "\nBlock Data: " + str(
            self.list_of_submissions) + "\nHashes: " + str(self.nonce) +\
               "\n Number of submissions: " + str(self.submissions_num) + "\n-----------------------"




class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Blockchain(object):
    __metaclass__ = Singleton

    def __init__(self):
        # self.__difficulty = 15
        self.__difficulty = 12
        self.__maximum_valid_nonce = 2 ** 32
        self.__target_nonce = 2 ** (256 - self.__difficulty)
        self.__blockchain_height = 0

        self.__head_block = Block([])
        self.__head_block.previous_hash = "Genesis block"
        _ = self.__genesis_block = self.__head_block

        self._instance = self
        self.lock = Lock()


    def __getitem__(self, block_hash):
        current_block = self.genesis_block
        while current_block is not None:
            if current_block.block_hash == block_hash:
                return current_block
            current_block = current_block.next

    @property
    def difficulty(self):
        return self.__difficulty

    @property
    def maximum_valid_nonce(self):
        return self.__maximum_valid_nonce

    @property
    def target_nonce(self):
        return self.__target_nonce

    @property
    def genesis_block(self):
        return self.__genesis_block

    @property
    def head_block(self):
        return self.__head_block

    @head_block.setter
    def head_block(self, block):
        self.__head_block = block

    @property
    def blockchain_height(self):
        return self.__blockchain_height

    def data_already_submitted(self, submission):
        current_block = self.genesis_block
        while current_block is not None:
            if submission in current_block.list_of_submissions:
                return True
            current_block = current_block.next
        return False

    def is_blockchain_valid(self):
        current_block = self.genesis_block
        while current_block is not None:
            previous_block_hash = current_block.hash()
            current_block = current_block.next
            if current_block is not None:
                if current_block.previous_hash != previous_block_hash:
                    return False
        return True


    def add(self, block):
        with self.lock:
            block.previous_hash = self.head_block.hash()
            block.block_num = self.head_block.block_num + 1

            self.head_block.next = block
            self.head_block = self.head_block.next

            self.__blockchain_height += 1
