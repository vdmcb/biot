from Blockchain import signature_validator
from Blockchain.data_pool import DataPackage

from threading import Thread
import random


class DataGenerator(Thread):
    def __init__(self, data_pool):
        Thread.__init__(self)
        self.data_pool = data_pool
        self.num_of_generated_keys = 1
        self.keys = []
        self.generate_keys()

    def generate_keys(self):
        for _ in range(self.num_of_generated_keys):
            private_key, public_key = signature_validator.generate_keys()
            self.keys.append((private_key, public_key))

    def run(self):
        while True:
            if len(self.keys) > 0:
                selected_keys = random.choice(self.keys)
                data = DataPackage(selected_keys[1], random.random())
                data.sign_package(selected_keys[0])
                data = data._pack_to_json()
                self.data_pool.submit_data(data)
