from DataHandling.logger import logger
from DataHandling.network_manipulator import get_ips_and_ports

from threading import Thread, Lock, Event
import socket


class BroadcastingModule(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.lock = Lock()

        self.ips_and_ports = []
        self._read_ips_and_ports()

        self.listeners = []
        self._create_listeners()

        self.received_block = None
        self.block_received = Event()

        self.start()

    def run(self):
        self._start_listeners()

    def _read_ips_and_ports(self):
        # Read available ips_and_ports from DB
        # List of tuples (ip, port)
        self.ips_and_ports = get_ips_and_ports()

    def broadcast_block(self, block):
        senders = []
        for ip, port in self.ips_and_ports:
            senders.append(Sender(ip, port, str(block)))

        if len(senders) > 0:
            logger.debug_message("Sender threads were initialized.")
        else:
            logger.debug_message("There are no available nodes for block broadcasting.")
            return

        for sender in senders:
            sender.start()
        logger.debug_message("Sender threads were started.")

    def set_received_block(self, block):
        if not self.block_received.is_set():
            self.block_received.set()
            with self.lock:
                self.received_block = block

    def get_received_block(self):
        with self.lock:
            if self.received_block is not None:
                # The received block will be validated so there's no point in keeping the copy
                self.block_received.clear()
                return self.received_block
            return None

    def _create_listeners(self):
        for ip, port in self.ips_and_ports:
            self.listeners.append(Listener(ip, port, self))

        if len(self.listeners) > 0:
            logger.debug_message("Listener threads were initialized.")

    def _start_listeners(self):
        if not len(self.listeners) > 0:
            logger.debug_message("There are no available broadcasting nodes.")
            return

        for listener in self.listeners:
            listener.start()

        logger.debug_message("Listener threads were started.")


class Listener(Thread):
    def __init__(self, ip, port, broadcasting_module):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.broadcasting_module = broadcasting_module
        self.listener_socket = None

        self.create_connection()

    def run(self):
        if self.listener_socket is not None:
            self.listener_socket.listen(1)
            while True:
                connection, address = self.listener_socket.accept()

                while True and connection is not None:
                    block = connection.recv(1024)
                    if not block:
                        break

                    if len(str(block)) > 0:
                        self.broadcasting_module.set_received_block(str(block))

    def create_connection(self):
        try:
            self.listener_socket = socket.socket()
            self.listener_socket.bind((self.ip, self.port))

        except:
            self.listener_socket = None
            logger.error_message("Could not create listener socket while trying to connect to available nodes.")


class Sender(Thread):
    def __init__(self, ip, port, block):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.sender_socket = None
        self.block = block

        self.create_connection()

    def run(self):
        if self.block is not None and len(str(self.block)) > 0:
            self.send_block(self.block)

    def create_connection(self):
        try:
            self.sender_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sender_socket.connect((self.ip, self.port))

        except:
            logger.error_message("Could not create sender socket while trying to broadcast a block.")
            self.sender_socket = None

    def send_block(self, block):
        if self.sender_socket is not None:
            try:
                self.sender_socket.sendall(str(block))
                self.sender_socket.close()

            except:
                logger.error_message("An unexpected error was encountered while broadcasting a block.")
