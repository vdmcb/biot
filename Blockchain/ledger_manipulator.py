from Blockchain.signature_validator import stored_keys_available, get_stored_keys
from constants import ledger_file_name
from DataHandling.logger import logger
import pickle
import pyAesCrypt
import os


def save_ledger(blockchain):
    try:
        with open(ledger_file_name, mode='wb') as ledger_file:
            pickle.dump(blockchain, ledger_file)
            return True
    except:
        return False

def encrypt_ledger():
    private_key = None
    if stored_keys_available():
        private_key, _ = get_stored_keys()
    if private_key is None:
        logger.debug_message('Stored keys not found.')
        return

    buffer_size = 64 * 1024
    if os.path.exists(os.path.join(os.getcwd(), ledger_file_name)):
        try:
            pyAesCrypt.encryptFile(ledger_file_name, ledger_file_name + '.aes', private_key, buffer_size)
            os.remove(os.path.join(os.getcwd(), ledger_file_name))
        except:
            logger.error_message('Could not encrypt ledger.')

def decrypt_ledger():
    private_key = None
    if stored_keys_available():
        private_key, _ = get_stored_keys()
    if private_key is None:
        logger.error_message('Stored keys not found.')
        return

    buffer_size = 64 * 1024
    if os.path.exists(os.path.join(os.getcwd(), ledger_file_name + '.aes')):
        try:
            pyAesCrypt.decryptFile(ledger_file_name + '.aes', ledger_file_name, private_key, buffer_size)
        except:
            logger.error_message('Could not decrypt ledger.')

def retrieve_ledger():
    if os.path.exists(os.path.join(os.getcwd(), ledger_file_name)):
        try:
            with open(ledger_file_name, mode='rb') as ledger_file:
                return pickle.load(ledger_file_name)
        except:
            logger.error_message('Could not deserialize ledger.')
            return None
