from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from uuid import getnode as get_mac
from constants import stored_keys_file_name
from Blockchain.data_pool import DataPackage
import os


# Generating the keys
def generate_keys():
    random_generator = Random.new().read
    keys = RSA.generate(1024, random_generator)

    encrypted_keys = keys.export_key(passphrase=str(get_mac()), pkcs=8, protection="scryptAndAES128-CBC")
    with open(stored_keys_file_name, mode='wb') as keys_file:
        keys_file.write(encrypted_keys)

    return keys, keys.publickey()

def get_stored_keys():
    if os.path.exists(os.path.join(os.getcwd(), stored_keys_file_name)):
        encrypted_keys = None
        with open(stored_keys_file_name, mode='rb') as keys_file:
            encrypted_keys = keys_file.read()
        if encrypted_keys is not None:
            key = RSA.import_key(encrypted_keys, passphrase=str(get_mac()))
            return key, key.publickey()
    return None, None

def stored_keys_available():
    if os.path.exists(os.path.join(os.getcwd(), stored_keys_file_name)):
        if os.stat(stored_keys_file_name).st_size > 0:
            return True
    return False

def sign_package(transaction, private_key):
    # Hashing the transaction
    transaction_hash = SHA256.new(str(transaction).encode())

    # Signing the transaction
    digital_pen = PKCS1_v1_5.new(private_key)
    signature = digital_pen.sign(transaction_hash)

    return signature

def validate_signature(transaction, public_key, signature):
    # Hashing the transaction
    transaction_hash = SHA256.new(str(transaction).encode())

    # Validating transaction
    verifier = PKCS1_v1_5.new(public_key)
    if verifier.verify(transaction_hash, signature):
        return True
    return False


def get_valid_transactions(submissions, blockchain):
    valid_transactions = []
    # Hashing the transaction
    if submissions is not None:
        for data_submission in submissions:
            data_submission = DataPackage.json_to_data_package(data_submission)
            if validate_signature(data_submission, data_submission.public_key, data_submission.signature) \
                    and not blockchain.data_already_submitted(data_submission):
                valid_transactions.append(data_submission)
        return valid_transactions

# Formatting for console output
# hexificator = codecs.getencoder('hex')
# print('Signed: ', ((hexificator(signature))[0]))

# get_stored_keys()