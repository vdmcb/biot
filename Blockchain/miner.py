from Blockchain.blockchain import Block
from Blockchain import signature_validator
from DataHandling.logger import logger
from Blockchain.broadcasting_module import BroadcastingModule

from threading import Thread


class Miner(Thread):
    def __init__(self, miner_id, blockchain, data_pool):
        Thread.__init__(self)
        self.data_pool = data_pool
        self.miner_id = miner_id
        self.blockchain = blockchain

        self.broadcasting_module = BroadcastingModule()



    def run(self):
        while True:
            pending_submissions = self.data_pool.retrieve_random_data_package()
            if pending_submissions is not None:
                # These submissions will be mined, as long as nobody mines another block faster
                successfuly_mined = self.mine(pending_submissions)
                while not successfuly_mined:
                    successfuly_mined = self.mine(pending_submissions)

    def mine(self, submissions):
        valid_transactions = signature_validator.get_valid_transactions(submissions, self.blockchain)
        # Creating block that will be mined
        block = Block(valid_transactions)

        for _ in range(self.blockchain.maximum_valid_nonce):
            # Finding valid hash
            if not self.broadcasting_module.block_received.is_set():
                if int(block.hash(), 16) <= self.blockchain.target_nonce:
                    block.block_hash = block.hash()
                    self.blockchain.add(block)

                    logger.debug_message("MINING:\n" + str(block))
                    return True
                else:
                    block.nonce += 1

            else:
                self.validate_received_block(self.broadcasting_module.get_received_block())
                return False


    def validate_received_block(self, block):
        # This block was received from another node and needs to be validated before it can be added to the blockchain
        pass
