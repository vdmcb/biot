from Blockchain import signature_validator

from Crypto.PublicKey import RSA
import requests
import datetime
import json
import binascii

class DataPackage:
    __signature = None
    # def __init__(self, sender_address, recipient_address, public_key, data):

    def __init__(self, public_key, data):
        self.public_key = public_key
        # self.recipient_address = recipient_address
        self.data = data
        self.public_key = public_key
        self.date = datetime.datetime.now()

    def sign_package(self, private_key):
        self.__signature = signature_validator.sign_package(str(self), private_key)

    def _pack_to_json(self):
        data = {"Public key": self.public_key.export_key(), "data": str(self.data),
                "date": str(self.date), "signature": binascii.hexlify(self.signature).decode('utf-8')}

        return data

    @staticmethod
    def json_to_data_package(serialized_data):
        data_package = DataPackage(RSA.import_key(serialized_data['Public key']), serialized_data['data'])
        data_package.date = serialized_data['date']
        data_package.__signature = binascii.unhexlify(serialized_data['signature'].encode('utf-8'))

        return data_package

    @property
    def signature(self):
        return self.__signature

    def __str__(self):
        data = {"Public key": self.public_key.export_key(), "data": str(self.data),
                "date": str(self.date)}
        return str(data)


class DataPool:
    _instance = None

    def __init__(self):
        if self._instance is None:
            self._instance = self
            self._url = 'http://127.0.0.1'
            self._port = '9987'
        else:
            raise Exception('Singleton. Cannot create another instance.')

    def url(self):
        return self._url + ':' + self._port

    @property
    def instance(self):
        if self._instance is None:
            DataPool()
        return self._instance

    def submit_data(self, data):
        response = requests.post(self.url(), data=data)
        return response.status_code == 200

    # The data is already shuffled so the transaction selection order is kept arbitrary
    def retrieve_random_data_package(self):
        response = requests.get(self.url())
        if response.status_code == 200:
            print(str(response.content))
            return json.loads(response.content)
        return None

