from Blockchain import signature_validator
from DataHandling.logger import logger
from Blockchain.data_pool import DataPackage
from constants import biot_client_receiver_port

from urllib import request
from threading import Thread
from queue import Queue
import socket
import time


class BiotClient(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.private_key = None
        self.public_key = None

        self.data_receiving_thread = None
        self.data_sending_thread = None
        self.data_queue = Queue()

        self.is_initialized = False
        if not self.is_initialized:
            self.initialize()

    def run(self):
        while True:
            if self.data_receiving_thread is None or self.data_sending_thread is None:
                self.initialize()
                time.sleep(0.5)
            if not self.data_receiving_thread.is_alive():
                logger.warning_message("Data receiving thread of BIOT Client is not alive. Reinitializing.")
                self._start_receiver_thread()
            if not self.data_sending_thread.is_alive():
                logger.warning_message("Data sending thread of BIOT Client is not alive. Reinitializing.")
                self._start_sender_thread()
            time.sleep(2)

    def initialize(self):
        if self.internet_connection():
            if not signature_validator.stored_keys_available():
                signature_validator.generate_keys()

            elif signature_validator.stored_keys_available():
                self.is_initialized = True
                self.private_key, self.public_key = signature_validator.get_stored_keys()

                if self.private_key is None or self.public_key is None:
                    logger.error_message("Keys are None after initialization. Shutting down the client. Please restart.")
                    return
            else:
                logger.warning_message("Could not initialize the BIOT client.")

            # @TODO: Should retrieve the data pool address

            self._start_receiver_thread()
            self._start_sender_thread()
            self.is_initialized = True

        else:
            logger.warning_message("Could not connect the BIOT Client to the internet.")

    def _start_sender_thread(self):
        self.data_sending_thread = Thread(target=self.data_sender)
        self.data_sending_thread.start()

    def _start_receiver_thread(self):
        self.data_receiving_thread = Thread(target=self.data_receiver)
        self.data_receiving_thread.start()

    def data_receiver(self):
            listener_socket = socket.socket()
            host = socket.gethostname()
            port = biot_client_receiver_port
            listener_socket.bind((host, port))

            listener_socket.listen(5)
            connection = None
            try:
                while True:
                    try:
                        connection, addr = listener_socket.accept()
                        logger.debug_message("BIOT client has connected to the sending socket.")
                        entire_data_package = []
                        while True:
                            try:
                                received_message = connection.recv(1024)
                                connection.settimeout(None)

                                if not received_message:
                                    raise socket.timeout

                                entire_data_package.append(received_message)

                            except socket.timeout:
                                logger.error_message("BIOT client data receiving socket has timed out.")
                                break

                        self.data_queue.put(''.join(entire_data_package))

                    except socket.error as e:
                        if e.errno == 10054:  # [Errno 10054] An existing connection was forcibly closed by the remote host
                            logger.error_message("The sender has disconnected. Waiting for 5 seconds before trying "
                                        "to reconnect")
                        else:
                            raise Exception
            except:
                logger.error_message("Something went wrong while listening to the data sending socket")
                if connection is not None:
                    connection.close()

    def data_sender(self):
        while True:
            if not self.data_queue.empty():
                data_to_be_sent = self.data_queue.get()
                self.submit_data(data_to_be_sent)
                time.sleep(5)

    def submit_data(self, data):
        packed_data = DataPackage(self.public_key, data)
        packed_data.sign_package(self.private_key)

        # @TODO: after the server which holds the data pool is up and running, finish implementing this

    @staticmethod
    def internet_connection():
        for timeout in [1, 5, 10, 15]:
            try:
                _ = request.urlopen('http://google.com', timeout=timeout)
                return True
            except Exception as err:
                pass
        return False


if __name__ == '__main__':
    biot_client = BiotClient()
    biot_client.start()
